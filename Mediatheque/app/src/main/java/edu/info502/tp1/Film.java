package edu.info502.tp1;

public class Film extends Media {
    private String realisateur;
    private int annee;

    // Constructeurs
    public Film() {
        super();
        this.realisateur = "";
        this.annee = 0;
    }

    public Film(String titre, String cote, int note, String realisateur, int annee) {
        super(titre, cote, note);
        this.realisateur = realisateur;
        this.annee = annee;
    }

    public Film(Film film) {
        super(film);
        this.realisateur = film.realisateur;
        this.annee = film.annee;
    }

    // Getters et Setters
    public String getRealisateur() {
        return realisateur;
    }

    public void setRealisateur(String realisateur) {
        this.realisateur = realisateur;
    }

    public int getAnnee() {
        return annee;
    }

    public void setAnnee(int annee) {
        this.annee = annee;
    }

    @Override
    public String toString() {
        return "Film{" +
                "titre='" + getTitre() + '\'' +
                ", cote=" + getCote() +
                ", note=" + getNote() +
                ", realisateur='" + realisateur + '\'' +
                ", annee=" + annee +
                '}';
    }
}
//Question9
/*public class Main {
    public static void main(String[] args) {
        Film film1 = new Film("Inception", "XYZ123", 9, "Christopher Nolan", 2010);
        System.out.println(film1);
    }
}
*/