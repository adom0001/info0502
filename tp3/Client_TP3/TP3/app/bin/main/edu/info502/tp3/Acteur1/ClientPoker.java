package edu.info502.tp3.Acteur1;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;
import org.json.JSONArray;
import org.json.JSONObject;

public class ClientPoker {
    private static final String serverAddress = "10.11.16.137";
    private static final int serverPort = 12345;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String nickname;

        // Demande du pseudo
        System.out.print("(Client)Entrée votre nom de joueur : ");
        nickname = scanner.nextLine().trim();

        try (Socket clientSocket = new Socket(serverAddress, serverPort);
             PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
             BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()))) {

            // Envoi du nickname au serveur
            out.println(nickname);

            // Gestion des messages serveur
            while (true) {
                String serverMessage = in.readLine();
                JSONObject serveurMess = new JSONObject(serverMessage);
                String key = serveurMess.getString("key");

                switch (key) {
                    case "nickname":
                        String serverResponse = serveurMess.getString("value");
                        System.out.println("(nickname)(Serveur) " + serverResponse);

                        // Si le serveur demande un autre pseudo (pseudo déjà pris)
                        if (serverResponse.contains("Ce pseudo est déjà pris")) {
                            System.out.print("(Client) Choisissez un autre pseudo : ");
                            nickname = scanner.nextLine().trim();
                            out.println(nickname); // Envoie du nouveau pseudo
                        }
                        scanner.close();
                        break;
                    case "start":
                        System.out.println("(start)(Serveur) " + serveurMess.getString("value"));
                        break;                    
                    case "hand":
                        System.out.println("(hand)(Serveur) Vous avez reçu vos cartes privées :");
                        JSONArray hand = serveurMess.getJSONArray("value");
                        for (int i = 0; i < hand.length(); i++) {
                            JSONObject carteobjet = hand.getJSONObject(i);
                            String carteString = carteobjet.getString("carte");
                            System.out.println("(Serveur) :"+carteString);
                        }
                        break;
                    case "flop":
                        System.out.println("(Client) Phase du Flop :");
                        JSONArray flop = serveurMess.getJSONArray("value");
                        for (int i = 0; i < flop.length(); i++) {
                            JSONObject carteobjet = flop.getJSONObject(i);
                            String carteString = carteobjet.getString("carte");
                            System.out.println("(Serveur) :"+carteString);
                        }
                        break;
                    case "turn":
                        System.out.println("(Client) Phase du Turn :");
                        JSONArray turn = serveurMess.getJSONArray("value");
                        for (int i = 0; i < turn.length(); i++) {
                            JSONObject carteobjet = turn.getJSONObject(i);
                            String carteString = carteobjet.getString("carte");
                            System.out.println("(Serveur) :"+carteString);
                        }                        
                        break;
                    case "river":
                        System.out.println("(Client) Phase du River :");
                        JSONArray river = serveurMess.getJSONArray("value");
                        for (int i = 0; i < river.length(); i++) {
                            JSONObject carteobjet = river.getJSONObject(i);
                            String carteString = carteobjet.getString("carte");
                            System.out.println("(Serveur) :"+carteString);
                        }                        
                        break;
                    case "resultat":
                        System.out.println("(Client) Les jeux sont faits !");
                        System.out.println("Votre main " + serveurMess.getString("value"));
                        break;
                    case "shutdown":
                        System.out.println("(Client) Fermeture demandée par le serveur.");
                        System.out.println(serveurMess.getString("value"));
                        return;
                    default:
                        System.out.println("(erreur)Message inconnu du serveur : " + serveurMess);
                        break;
                }
            }
        } catch (IOException e) {
            System.err.println("Erreur de communication avec le serveur : " + e.getMessage());
        }
    }
}
