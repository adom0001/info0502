package edu.info502.tp3.infra;

import java.util.ArrayList;
import java.util.Collections;

public class Paquet {
    private ArrayList<Card> paquetDeCarte;

    public Paquet() {
        this.paquetDeCarte = new ArrayList<>(52); // 52 cartes dans un paquet traditionnel
        for (Card.Symbole symbole : Card.Symbole.values()) {
            for (int i = 1; i <= 13; i++) { // de 1 (As) à 13 (Roi)
                paquetDeCarte.add(new Card(i, symbole));
            }
        }
    }

    public void shuffle() {
        Collections.shuffle(paquetDeCarte);
    }

    public Card getCarte(int index) {
        return paquetDeCarte.get(index);
    }

    public int getSize() {
        return paquetDeCarte.size();
    }

    public Card drawCard() {
        return paquetDeCarte.isEmpty() ? null : paquetDeCarte.remove(0);
    }
    public void Afficher(){
        for (int i = 0; i < getSize(); i++) {
            System.out.println(getCarte(i));
        }
    }
}

