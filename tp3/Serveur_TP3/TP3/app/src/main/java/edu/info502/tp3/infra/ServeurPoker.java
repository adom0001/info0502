package edu.info502.tp3.infra;

import java.io.*;
import java.net.*;
import java.util.*;
import org.json.JSONObject;

public class ServeurPoker {
    private static final int PLAYERS_MAX = 10;
    private static List<Player> ConnectedClient;
    private static Game playedGame; 
    private ServerSocket serverSocket;

    public ServeurPoker() {
        ConnectedClient = new ArrayList<>();
        playedGame = null;
    }

    public void LauchServer() {
        System.out.println("Server : Hello world !");
        System.out.println("Server : Server will start soon");
        try {
            InetAddress adresseServeur = InetAddress.getByName("10.11.16.137");
            serverSocket = new ServerSocket(12345, 50, adresseServeur);
            // Message pour dire que le serveur écoute bien
            System.out.println("---------------------------------------------------------------");
            System.out.println("Server : Server listenning...");
            System.out.println("Server : Server : IP :" + adresseServeur);
            System.out.println("Server : Port : 12345");
            System.out.println("Server : Connected : " + ConnectedClient.size());

            // Tant qu'on ne dépasse pas la limite de clients, on accepte les clients
            while (ConnectedClient.size() < PLAYERS_MAX - 1) {
                Socket socket = serverSocket.accept();
                System.out.println("---------------------------------------------------------------");
                System.out.println("Server : Connexion acceptée depuis " + socket.getInetAddress());
                Player player = new Player(socket);
                ConnectedClient.add(player);
                Thread threadPlayer = new Thread(player);
                threadPlayer.start();
                System.out.println("Server : Connected : " + ConnectedClient.size());

                // On attend que tous les joueurs aient un pseudo et accepté de jouer
                System.out.println("(Server) Nombre de personnes connectées avant la partie = " + ConnectedClient.size());
                while (ConnectedClient.size() >= 2 && playedGame == null) {
                    // Attendre que tous les joueurs aient un pseudo et accepté de jouer
                    while (!allPlayersHaveNicknames()) {
                        try {
                            Thread.sleep(500); // Pause pour éviter une surcharge CPU
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    playedGame = new Game(ConnectedClient);
                    playedGame.startGame();
                    System.out.println("Server : Fermeture des connexions des joueurs...");
                    playedGame.closeConnection();
                    stopServer();             
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private boolean allPlayersHaveNicknames() {
        for (Player player : ConnectedClient) {
            if (player.getNickname() == null) {
                return false;
            }
        }
        return true;
    }
    public void stopServer() {
        try {
            if (serverSocket != null && !serverSocket.isClosed()) {
                serverSocket.close();
                System.out.println("Server : Fermeture du serveur réussie.");
            }
        } catch (IOException e) {
            System.err.println("Server : Erreur lors de la fermeture du serveur : " + e.getMessage());
        }
    }
    
    public static void main(String[] args) {
        ServeurPoker serveurPoker = new ServeurPoker();
        serveurPoker.LauchServer();
    }

    // Classe interne Player pour gérer les joueurs
    public static class Player implements Runnable {
        private Socket socket;
        private PrintWriter out;
        private BufferedReader in;
        private String nickname;
        private String play;
        private Hand hand;

        public Player(Socket socket) {
            this.socket = socket;
            this.hand = new Hand();
        }

        public void run() {
            try {
                // Initialisation des flux
                this.in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                this.out = new PrintWriter(socket.getOutputStream(), true);

                String nicknameJSON;
                boolean pseudoValid;

                // Validation du pseudo
                do {
                    nicknameJSON = in.readLine();  // Lire le pseudo envoyé par le client
                    synchronized (ConnectedClient) {
                        pseudoValid = validateAndAddPlayer(nicknameJSON);
                    }

                    // Si le pseudo est déjà pris, envoyer une réponse
                    if (!pseudoValid) {
                        JSONObject rep = new JSONObject();
                        rep.put("key", "nickname");
                        rep.put("value", "Ce pseudo est déjà pris");
                        out.println(rep.toString());
                    } else {
                        // Sinon, accepter le pseudo et l'envoyer au joueur
                        this.nickname = nicknameJSON;
                        JSONObject rep = new JSONObject();
                        rep.put("key", "nickname");
                        rep.put("value", "Nickname = Available. Bienvenue à la table " + nicknameJSON);
                        out.println(rep.toString());
                    }
                } while (!pseudoValid);  
            } catch (IOException e) {
                System.err.println("Erreur dans la lecture du flux ou la connexion avec le client : " + e.getMessage());
                return;
            }
        }

        public void closeConnection() {
            try {
                in.close();
                out.close();
                socket.close();
                System.out.println("Connexion fermée pour le joueur : " + nickname);
            } catch (IOException e) {
                System.err.println("Erreur lors de la fermeture des ressources pour le joueur : " + nickname);
            }
        }

        private synchronized boolean validateAndAddPlayer(String nickname) {
            // Vérifier les doublons
            for (Player player : ConnectedClient) {
                if (player.getNickname() != null && player.getNickname().equalsIgnoreCase(nickname)) {
                    return false; // Pseudo déjà pris
                }
            }

            // Si le pseudo est valide, l'ajouter à la liste
            for (Player player : ConnectedClient) {
                if (player.getNickname() == null) {
                    player.nickname = nickname;
                    return true; // Validation réussie
                }
            }
            return false; // Aucun joueur disponible pour ce pseudo
        }
        public String getNickname() {
            return this.nickname;
        }

        public String getPlay() {
            return this.play;
        }

        public Hand getHand() {
            return this.hand;
        }

        public PrintWriter getOut() {
            return this.out;
        }

        public BufferedReader getIn() {
            return this.in;
        }
    }
}
