package edu.info502.tp3.infra; 

public class Card {
    protected int chiffre; // 1 = AS -> 13 = KING
    protected Symbole symbole; // TREFLE, COEUR, CARREAUX, PIQUE
    
        public static enum Symbole {
            TREFLE, COEUR, CARREAUX, PIQUE
        }
    
        // Constructeur par initialisation
        public Card(int chiffre, Symbole symbole) {
            this.chiffre = chiffre;
            this.symbole = symbole;
        }
    
        // Constructeur par défaut
        public Card() {
            this.chiffre = 1;
            this.symbole = Symbole.PIQUE;
        }
    
        // Getters et Setters
        public int getChiffre() {
            return chiffre;
        }
    
        public Symbole getSymbole() {
            return symbole;
    }

    public void setChiffre(int chiffre) {
        this.chiffre = chiffre;
    }

    public void setSymbole(Symbole symbole) {
        this.symbole = symbole;
    }

    @Override
    public String toString() {
        String res;

        switch (getChiffre()) {
            case 1:
                res = "As de " + getSymbole();
                break;
            case 11:
                res = "Le Valet de " + getSymbole();
                break;
            case 12:
                res = "La Dame de " + getSymbole();
                break;
            case 13:
                res = "Le Roi de " + getSymbole();
                break;
            default:
                res = "Le " + getChiffre() + " de " + getSymbole();
                break;
        }
        return res;
    }
    public String toJson(){
        return String.format("{\"chiffre\":%d,\"symbole\":\"%s\"}",chiffre,symbole.name());
    }
}
