package edu.info502.tp3.infra;

import java.io.PrintWriter;
import java.util.concurrent.TimeUnit;
import java.util.*;
import org.json.JSONArray;


import org.json.JSONObject;

public class Game {
    private List<ServeurPoker.Player> players;
    private Talon talon; // Talon utilisé pour piocher des cartes
    private List<Card> cartesCommunes; // Cartes communes (flop, turn, river)
    
    public Game(List<ServeurPoker.Player> players) {
        this.players = players;
        this.talon = new Talon();
        this.cartesCommunes = new ArrayList<>();
    }

    // Démarrer une nouvelle partie de poker
    public synchronized void startGame() {
        System.out.println("---------------------------------------------------------------");
        System.out.println("(Serveur)Début de la partie de Poker.");
        for(ServeurPoker.Player p : players){
            JSONObject startGame = new JSONObject();
            startGame.put("key", "start");
            startGame.put("value", "La partie de Poker commence !");
            p.getOut().println(startGame.toString()); 
        }
        System.out.println("---------------------------------------------------------------");
        System.out.println("(Serveur)Distribution des cartes communes...");
        distribuerCartesPrivees();
        //Attente
        System.out.println("---------------------------------------------------------------");
        System.out.println("(Serveur)Flop");
        distribuerFlop();
        try {
            attendre(2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("---------------------------------------------------------------");
        System.out.println("(Serveur)Turn");
        distribuerTurn();
        try {
            attendre(2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //Attente
        System.out.println("---------------------------------------------------------------");
        System.out.println("(Serveur)River");
        distribuerRiver();
        try {
            attendre(2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //Attente
        System.out.println("---------------------------------------------------------------");
        System.out.println("(Serveur)Comparaison des mains");
        evaluerMainsJoueurs();
        try {
            attendre(2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("---------------------------------------------------------------");
        System.out.println("(Serveur)Annonce des résultats");
        determinerGagnant();
        return;
    }

    // Distribuer deux cartes privées à chaque joueur toi
    
    private void distribuerCartesPrivees() {
        //On ajoute dans chaque main 2 cartes qu'on distribu chacune leur tour(1 pour toi, 1 pour toi...);
        System.out.println("(Serveur)Envoie de 2 cartes pour chaque joueurs");
        for(int i=0;i<2;i++){
            for(ServeurPoker.Player p : players){
                p.getHand().ajouterCarte(talon.draw());
            }
        }
        for(ServeurPoker.Player p : players){
            Hand handP = p.getHand();
            JSONObject hand = new JSONObject();
            JSONArray cardA = new JSONArray();
            for(Card card : handP.getCards()){
                JSONObject cardJSON = new JSONObject();
                cardJSON.put("carte",card.toString());
                cardA.put(cardJSON);
            }
            hand.put("key","hand");
            hand.put("value",cardA);
            try{
                PrintWriter out = p.getOut();
                out.println(hand.toString());
                System.out.println("(Serveur)Envoie de la main pour le joueur ");
            }catch(Exception e){
                e.printStackTrace();
            }
        }
        System.out.println("(Serveur)Tout les cartes ont été distribué aux joueurs !");
    }

    // Distribuer le Flop (3 cartes)
    private void distribuerFlop() {
        for(int i=0;i<3;i++){
            cartesCommunes.add(talon.draw());
        }
        for(ServeurPoker.Player p : players){
            JSONObject flop = new JSONObject();
            JSONArray cardA = new JSONArray();
            for(Card card : cartesCommunes){
                JSONObject cardJSON = new JSONObject();
                cardJSON.put("carte",card.toString());
                cardA.put(cardJSON);
            }
            flop.put("key","flop");
            flop.put("value",cardA);
            try{
                PrintWriter out = p.getOut();
                out.println(flop.toString());
            }catch(Exception e){
                e.printStackTrace();
            }
        }
    }
    // Distribuer le Turn (1 carte)
    private void distribuerTurn() {
        cartesCommunes.add(talon.draw());
        
        for(ServeurPoker.Player p : players){
            JSONObject flop = new JSONObject();
            JSONArray cardA = new JSONArray();
            for(Card card : cartesCommunes){
                JSONObject cardJSON = new JSONObject();
                cardJSON.put("carte",card.toString());
                cardA.put(cardJSON);
            }
            flop.put("key","turn");
            flop.put("value",cardA);
            try{
                PrintWriter out = p.getOut();
                out.println(flop.toString());
            }catch(Exception e){
                e.printStackTrace();
            }
        }
    }

    // Distribuer le River (1 carte)
    private void distribuerRiver() {
        cartesCommunes.add(talon.draw());
        for(ServeurPoker.Player p : players){
            JSONObject flop = new JSONObject();
            JSONArray cardA = new JSONArray();
            for(Card card : cartesCommunes){
                JSONObject cardJSON = new JSONObject();
                cardJSON.put("carte",card.toString());
                cardA.put(cardJSON);
            }
            flop.put("key","river");
            flop.put("value",cardA);
            try{
                PrintWriter out = p.getOut();
                out.println(flop.toString());
            }catch(Exception e){
                e.printStackTrace();
            }
        }
    }

    //Évaluer les mains des joueurs en combinant cartes privées et communes
    private void evaluerMainsJoueurs() {
        for (ServeurPoker.Player player : players) {
            // Création de la main complète (cartes privées + cartes communes)
            Hand mainComplete = new Hand();
            mainComplete.Hand.addAll(player.getHand().Hand);
            mainComplete.Hand.addAll(cartesCommunes);
            // Trier et évaluer la main complète
            mainComplete.Hand.sort((c1, c2) -> Integer.compare(c1.getChiffre(), c2.getChiffre()));

            String evaluation = mainComplete.eval();
            player.getHand().Hand= mainComplete.getCards();
            System.out.println(player.getHand().getCards().toString());
            // Préparer et envoyer le message pour ce joueur
            JSONObject res = new JSONObject();
            res.put("key", "resultat");
            res.put("value", evaluation);
            try {
                PrintWriter out = player.getOut();
                out.println(res.toString());
                System.out.println("(Serveur) Résultat envoyé au joueur " + player.getNickname() + " : " + evaluation);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    

    //Déterminer le gagnant parmi les joueurs
    private void determinerGagnant() {
        ServeurPoker.Player meilleurJoueur = null;
        int meilleurScore = -1;
    
        for (ServeurPoker.Player player : players) {
            Hand hand = player.getHand();
            int scoreActuel = hand.evalScore();
            if (meilleurJoueur == null || scoreActuel > meilleurScore) {
                meilleurJoueur = player;
                meilleurScore = scoreActuel;
            } else if (scoreActuel == meilleurScore) {
                // Comparer les kicker cards
                if (meilleurJoueur.getHand().evalKick() < hand.evalKick()) {
                    meilleurJoueur = player;
                }
            }
        }

        System.out.println("Le gagnant est " + meilleurJoueur.getNickname() + " avec le score : " + meilleurScore);
    }
    public void closeConnection(){
        for(ServeurPoker.Player player : players){
            String value = "La partie est terminée, le serveur va se fermer!";
            JSONObject shut = new JSONObject();
            shut.put("key", "shutdown");
            shut.put("value", value);
            try {
                PrintWriter out = player.getOut();
                out.println(shut.toString());
                System.out.println("(Serveur) Notifications de fermeture du serveur envoyé au joueurs");
            } catch (Exception e) {
                e.printStackTrace();
            }
            player.closeConnection();
        }
    }    

    private void attendre(int secondes) throws InterruptedException {
        TimeUnit.SECONDS.sleep(secondes);
    }
}
