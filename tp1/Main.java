public class Main {
    /*public static void main(String[] args) {
        Film film1 = new Film("Inception", "XYZ123", 9, "Christopher Nolan", 2010);
        System.out.println(film1);
    }*/
    public static void main(String[] args ) {
        Mediatheque mediatheque1 = new Mediatheque();
        Mediatheque mediatheque2 = new Mediatheque("Bob Lenon");
        Mediatheque mediatheque3 = new Mediatheque(mediatheque2);
        System.out.println(mediatheque2.getNomProprietaire());
        mediatheque1.setNomProprietaire("Patrick Sebastien");
        mediatheque3.displayMedias();
        
    }
}
