import java.util.ArrayList; // Importer ArrayList au lieu de Vector;
import java.util.List; // Importer List pour utiliser une interface

public class Mediatheque {
    private String nomProprietaire;
    private final List<Media> medias; // Utiliser List pour stocker les médias

    // Constructeurs
    public Mediatheque() {
        this.nomProprietaire = "";
        this.medias = new ArrayList<>(); // Initialiser avec ArrayList
    }

    public Mediatheque(String nomProprietaire) {
        this.nomProprietaire = nomProprietaire;
        this.medias = new ArrayList<>(); // Initialiser avec ArrayList
    }

    public Mediatheque(Mediatheque mediatheque) {
        this.nomProprietaire = mediatheque.nomProprietaire;
        this.medias = new ArrayList<>(mediatheque.medias); // Initialiser avec ArrayList
    }

    // Méthode pour ajouter un média
    public void add(Media media) {
        medias.add(media);
    }

    // Méthode pour afficher les médias
    public void displayMedias() {
        for (Media media : medias) {
            System.out.println(media);
        }
    }

    // Getters et Setters
    public String getNomProprietaire() {
        return nomProprietaire;
    }

    public void setNomProprietaire(String nomProprietaire) {
        this.nomProprietaire = nomProprietaire;
    }

    @Override
    public String toString() {
        return "Mediatheque{" +
                "nomProprietaire='" + nomProprietaire + '\'' +
                ", medias=" + medias +
                '}';
    }
}
