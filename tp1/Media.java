public class Media {
    private String titre;
    private StringBuffer cote;
    private int note;
    private static String nom; // Nom de la médiathèque

    // Constructeur par défaut
    public Media() {
        this.titre = "";
        this.cote = new StringBuffer();
        this.note = 0;
    }

    // Constructeur par initialisation
    public Media(String titre, String cote, int note) {
        this.titre = titre;
        this.cote = new StringBuffer(cote);
        this.note = note;
    }

    // Constructeur par copie
    public Media(Media media) {
        this.titre = media.titre;
        this.cote = new StringBuffer(media.cote);
        this.note = media.note;
    }

    // Getters et Setters
    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public StringBuffer getCote() {
        return new StringBuffer(cote); // Retourne une copie pour éviter les modifications
    }

    public void setCote(String cote) {
        this.cote = new StringBuffer(cote);
    }

    public int getNote() {
        return note;
    }

    public void setNote(int note) {
        this.note = note;
    }

    // Méthodes de classe
    public static void setNom(String nom) {
        Media.nom = nom;
    }

    public static String getNom() {
        return nom;
    }

    // Masquage des méthodes
    @Override
    public String toString() {
        return "Media{" +
                "titre='" + titre + '\'' +
                ", cote=" + cote +
                ", note=" + note +
                ", nom='" + nom + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof Media)) return false;
        Media media = (Media) obj;
        return note == media.note &&
                titre.equals(media.titre) &&
                cote.toString().equals(media.cote.toString());
    }

    @Override
    public int hashCode() {
        int result = titre.hashCode();
        result = 31 * result + cote.toString().hashCode();
        result = 31 * result + note;
        return result;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        Media clonedMedia = (Media) super.clone(); // Appel à super.clone()
        clonedMedia.cote = new StringBuffer(this.cote); // Cloner le StringBuffer
        return clonedMedia;
    }
    public static void main(String[] args) {
        
    }
}
