package edu.info502.tp2.poker;

import java.util.ArrayList;
import java.util.Collections;

public class Paquet {
    private ArrayList<Carte> paquetDeCarte;

    public Paquet() {
        this.paquetDeCarte = new ArrayList<>(52); // 52 cartes dans un paquet traditionnel
        for (Carte.Symbole symbole : Carte.Symbole.values()) {
            for (int i = 1; i <= 13; i++) { // de 1 (As) à 13 (Roi)
                paquetDeCarte.add(new Carte(i, symbole));
            }
        }
    }

    public void shuffle() {
        Collections.shuffle(paquetDeCarte);
    }

    public Carte getCarte(int index) {
        return paquetDeCarte.get(index);
    }

    public int getSize() {
        return paquetDeCarte.size();
    }

    public Carte drawCard() {
        return paquetDeCarte.isEmpty() ? null : paquetDeCarte.remove(0);
    }
    public void Afficher(){
        for (int i = 0; i < getSize(); i++) {
            System.out.println(getCarte(i));
        }
    }
}
