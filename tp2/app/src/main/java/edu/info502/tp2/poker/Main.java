package edu.info502.tp2.poker;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        // Mise en place d'un talon avec 4 paquets de cartes
        System.out.println("Mise en place d'un talon de 4 paquets de cartes mélangés");
        Talon talon = new Talon(4);
        
        // Simulation d'un jeu de poker avec 4 joueurs
        System.out.println("\nDébut de la simulation d'un jeu de poker avec 4 joueurs");

        // Création des mains pour chaque joueur
        Hand player1 = new Hand();
        Hand player2 = new Hand();
        Hand player3 = new Hand();
        Hand player4 = new Hand();

        // Distribution des cartes une par une
        System.out.println("\nDistribution des cartes :");
        for (int i = 0; i < 5; i++) { // Chaque joueur reçoit 5 cartes
            // Chaque joueur reçoit une carte à chaque tour
            player1.ajouterCarte(talon.draw());
            player2.ajouterCarte(talon.draw());
            player3.ajouterCarte(talon.draw());
            player4.ajouterCarte(talon.draw());
        }

        // Affichage des mains des joueurs après distribution
        System.out.println("\nMain du Joueur 1 :");
        player1.Main.forEach(System.out::println);
        System.out.println("Évaluation : " + player1.eval());

        System.out.println("\nMain du Joueur 2 :");
        player2.Main.forEach(System.out::println);
        System.out.println("Évaluation : " + player2.eval());

        System.out.println("\nMain du Joueur 3 :");
        player3.Main.forEach(System.out::println);
        System.out.println("Évaluation : " + player3.eval());

        System.out.println("\nMain du Joueur 4 :");
        player4.Main.forEach(System.out::println);
        System.out.println("Évaluation : " + player4.eval());
    
    //Texas hold'em
    // Initialisation du talon de cartes avec 4 jeux de cartes
        System.out.println("Mise en place d'un talon de 4 paquets de cartes mélangés");
        Talon talon2 = new Talon(4);  // On utilise 4 jeux de cartes

        // Création des mains des 4 joueurs
        Hand p1 = new Hand();
        Hand p2 = new Hand();
        Hand p3 = new Hand();
        Hand p4 = new Hand();

        // Distribution des cartes privées aux joueurs (2 cartes par joueur)
        System.out.println("Distribution des cartes privées aux joueurs");
        for (int i = 0; i < 2; i++) {
            p1.ajouterCarte(talon2.draw());
            p2.ajouterCarte(talon2.draw());
            p3.ajouterCarte(talon2.draw());
            p4.ajouterCarte(talon2.draw());
        }

        // Affichage des cartes privées des joueurs
        System.out.println("Cartes privées des joueurs :");
        afficherMainJoueur(1, p1);
        afficherMainJoueur(2, p2);
        afficherMainJoueur(3, p3);
        afficherMainJoueur(4, p4);

        // Distribution des cartes communes : Flop, Turn et River
        System.out.println("Distribution des cartes communes");
        ArrayList<Carte> cartesCommunes = new ArrayList<>();

        // Flop (3 cartes)
        System.out.println("\n*** Le Flop (3 cartes) ***");
        for (int i = 0; i < 3; i++) {
            cartesCommunes.add(talon2.draw());
        }
        afficherCartesCommunes(cartesCommunes);

        // Turn (1 carte)
        System.out.println("\n*** Le Turn (1 carte) ***");
        cartesCommunes.add(talon2.draw());
        afficherCartesCommunes(cartesCommunes);

        // River (1 carte)
        System.out.println("\n*** Le River (1 carte) ***");
        cartesCommunes.add(talon2.draw());
        afficherCartesCommunes(cartesCommunes);

        // Évaluation des mains des joueurs avec les cartes communes
        System.out.println("Évaluation des mains des joueurs...");
        evaluerMain(p1, cartesCommunes);
        evaluerMain(p2, cartesCommunes);
        evaluerMain(p3, cartesCommunes);
        evaluerMain(p4, cartesCommunes);

        // Comparaison des mains des joueurs pour déterminer le gagnant
        System.out.println("Comparaison des mains des joueurs");
        comparerMains(p1, p2, p3, p4);
    }

    // Méthode pour afficher la main d'un joueur (cartes privées)
    private static void afficherMainJoueur(int joueur, Hand main) {
        System.out.println("Joueur " + joueur + " : ");
        for (Carte carte : main.Main) {
            System.out.println("  " + carte);
        }
    }

    // Méthode pour afficher les cartes communes
    private static void afficherCartesCommunes(ArrayList<Carte> cartesCommunes) {
        System.out.print("Cartes communes : ");
        for (Carte carte : cartesCommunes) {
            System.out.print(carte + "  ");
        }
        System.out.println();
    }

    // Méthode pour évaluer la main d'un joueur avec les cartes communes
    private static void evaluerMain(Hand main, ArrayList<Carte> cartesCommunes) {
        if (main.Main.size() < 5) {
            for (Carte carte : cartesCommunes) {
                if (main.Main.size() < 5) {
                    main.ajouterCarte(carte);
                }
            }
        }

        // Tri des cartes de la main du joueur
        main.Main.sort((c1, c2) -> Integer.compare(c1.getChiffre(), c2.getChiffre()));

        // Évaluation de la main du joueur
        String evaluation = main.eval();
        System.out.println("Évaluation : " + evaluation);
    }

    // Méthode pour comparer les mains des joueurs et déterminer le gagnant
    private static void comparerMains(Hand... mains) {
        Hand meilleurJoueur = mains[0];
        int meilleureMain = meilleurJoueur.evalScore();
        int gagnant = 1;

        // Comparaison des mains des joueurs
        for (int i = 1; i < mains.length; i++) {
            int scoreCourant = mains[i].evalScore();
            if (meilleureMain < scoreCourant) { 
                meilleurJoueur = mains[i];
                meilleureMain = scoreCourant;
                gagnant = i + 1; 
            } else if (meilleureMain == scoreCourant) {
                //Comparer les cartes restantes (par exemple, les kicker cards)
                if (meilleurJoueur.evalKick() < mains[i].evalKick()) {
                    meilleurJoueur = mains[i];
                    gagnant = i + 1;
                }
            }
        }

        // Affichage du gagnant
        System.out.println("Le gagnant est le Joueur " + gagnant + " avec la main : " + meilleurJoueur.eval());
        afficherMainJoueur(gagnant, meilleurJoueur); 
    }
}
