package edu.info502.tp2.poker;

import java.util.LinkedList;
public class Talon {
    protected LinkedList<Carte> talon;

    public Talon(int nbJeu) {
        this.talon = new LinkedList<>(); // Correction : utilise LinkedList correctement
        for (int i = 0; i < nbJeu; i++) {
            Paquet p = new Paquet();
            p.shuffle();
            for(int j = 0;j<52;j++){
                talon.add(p.getCarte(j));
            }
        }
    }

    public Talon() {
        this.talon = new LinkedList<>(); // Correction : utilise LinkedList correctement
        Paquet p = new Paquet();
        p.shuffle();
        for(int j = 0;j<52;j++){
            talon.add(p.getCarte(j));
        }
    }

    public int getSize() {
        return talon.size();
    }
    
    public Carte draw(){
        return talon.poll();
    }

    public void afficherTalon() {
        System.out.println("Contenu du talon:");
        for (Carte carte : talon) {
            System.out.println(carte);
        }
    }
}
