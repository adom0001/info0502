package edu.info502.tp5.infra; 

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class Hand {
    protected ArrayList<Card> Hand;
    
    //consitution d'une main lorsque l'on pioche à la suite
    public Hand(Talon talon) {
        this.Hand = new ArrayList<>(5);
        for (int i = 0; i < 5; i++) {
            Hand.add(talon.draw());
        }
    }

    //Par défaut il n'y a pas de cartes pour pouvoir les distribués une par une par exemple 
    public Hand() {
        this.Hand = new ArrayList<>(5);
    }

    public ArrayList<Card> getCards(){
        return Hand;
    }

    public String toJson(){
        StringBuffer jsonBuilder = new StringBuffer("[");
        for(int i=0;i < Hand.size();++i){
            jsonBuilder.append(Hand.get(i).toJson());
            if(i < Hand.size()-1){
                jsonBuilder.append(",");
            }
        }
        jsonBuilder.append("]");
        return jsonBuilder.toString();
    }

    // Méthode pour ajouter une carte à la main
    public void ajouterCarte(Card card) {
        if (Hand.size() < 5) { // Vérifie qu'on n'ajoute pas plus de 5 cartes
            Hand.add(card);
        } else {
            System.err.println("La main est déjà pleine.");
        }
    }

    public ArrayList<Integer> getValeursCartes() {
        ArrayList<Integer> valeurs = new ArrayList<>();
        for (Card card : Hand) {
            valeurs.add(card.getChiffre());
        }
        return valeurs;
    }

    public ArrayList<Card.Symbole> getSymbolesCartes() {
        ArrayList<Card.Symbole> symboles = new ArrayList<>();
        for (Card card : Hand) {
            symboles.add(card.getSymbole());
        }
        return symboles;
    }

    public String eval() {
        Collections.sort(Hand, (c1, c2) -> Integer.compare(c1.getChiffre(), c2.getChiffre()));
        if (isRoyalFlush()) return "Quinte Flush Royale";
        if (isStraightFlush()) return "Quinte Flush";
        if (isFourOfAKind()) return "Carré";
        if (isFullHouse()) return "Full";
        if (isFlush()) return "Couleur";
        if (isStraight()) return "Suite";
        if (isThreeOfAKind()) return "Brelan";
        if (isTwoPair()) return "Double Paire";
        if (isOnePair()) return "Paire";
        return "Haute Carte";   
    }

    private boolean isRoyalFlush() {
        return isFlush() && isStraight() && getValeursCartes().contains(12); // La plus haute carte est un Roi (12)
    }

    private boolean isStraightFlush() {
        return isFlush() && isStraight();
    }

    private boolean isFourOfAKind() {
        return getOccurrences().containsValue(4);
    }

    private boolean isFullHouse() {
        Map<Integer, Integer> occurrences = getOccurrences();
        return occurrences.containsValue(3) && occurrences.containsValue(2);
    }

    private boolean isFlush() {
        Card.Symbole premierSymbole = Hand.get(0).getSymbole();
        for (Card card : Hand) {
            if (card.getSymbole() != premierSymbole) return false;
        }
        return true;
    }

    private boolean isStraight() {
        ArrayList<Integer> valeurs = getValeursCartes();
        for (int i = 1; i < valeurs.size(); i++) {
            if (valeurs.get(i) != valeurs.get(i - 1) + 1) return false;
        }
        return true;
    }

    private boolean isThreeOfAKind() {
        return getOccurrences().containsValue(3);
    }

    private boolean isTwoPair() {
        int count = 0;
        for (int occurrence : getOccurrences().values()) {
            if (occurrence == 2) count++;
        }
        return count == 2;
    }

    private boolean isOnePair() {
        return getOccurrences().containsValue(2);
    }

    // Compte les occurrences de chaque valeur dans la main
    private Map<Integer, Integer> getOccurrences() {
        Map<Integer, Integer> occurrences = new HashMap<>();
        for (Card card : Hand) {
            int valeur = card.getChiffre();
            occurrences.put(valeur, occurrences.getOrDefault(valeur, 0) + 1);
        }
        return occurrences;
    }

    //Pour le texas Holdem on introduit 2 nouvelle méthodes
    public int evalScore() {
        // On attribue un score en fonction du type de main (par exemple : 0 pour Haute Carte, 1 pour Paire, etc.)
        if (isRoyalFlush()) return 10;
        if (isStraightFlush()) return 9;
        if (isFourOfAKind()) return 8;
        if (isFullHouse()) return 7;
        if (isFlush()) return 6;
        if (isStraight()) return 5;
        if (isThreeOfAKind()) return 4;
        if (isTwoPair()) return 3;
        if (isOnePair()) return 2;
        return 1; // Haute Carte
    }
    
    public int evalKick() {
        // Cette méthode renvoie un score basé sur les cartes restantes (les "kick cards") qui sont utilisées pour départager des mains égales.
        ArrayList<Integer> valeurs = getValeursCartes();
        Collections.sort(valeurs, Collections.reverseOrder()); // Trie les valeurs par ordre décroissant
        int kickScore = 0;
        for (int i = 0; i < 5; i++) {
            kickScore += valeurs.get(i) * Math.pow(13, i); // Chaque carte est "multipliée" par une puissance de 13
        }
        return kickScore;
    }  

}
