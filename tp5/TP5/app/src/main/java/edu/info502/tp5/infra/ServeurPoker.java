package edu.info502.tp5.infra;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.JSONObject;

public class ServeurPoker {
    private static final int PLAYERS_MAX = 8;
    private static final int PLAYERS_MIN = 2; // Nombre minimum de joueurs pour démarrer la partie
    private static Map<String, Table> tables;
    private MqttClient mqttClient;

    public ServeurPoker() {
        tables = new HashMap<>();
    }

    public void LauchServer() {
        System.out.println("Server: Hello world!");
        System.out.println("Server: Server will start soon");
        try {
            mqttClient = new MqttClient("tcp://10.11.16.137:1883", MqttClient.generateClientId());
            MqttConnectOptions connOpts = new MqttConnectOptions();
            connOpts.setCleanSession(true);
            System.out.println("Connecting to broker: " + mqttClient.getServerURI());
            mqttClient.connect(connOpts);
            System.out.println("Connected");

            mqttClient.setCallback(new MqttCallback() {
                @Override
                public void connectionLost(Throwable cause) {
                    System.out.println("Connection lost: " + cause.getMessage());
                }

                @Override
                public void messageArrived(String topic, MqttMessage message) throws Exception {
                    System.out.println("Message arrived. Topic: " + topic + " Message: " + new String(message.getPayload()));
                    handleMessage(topic, new String(message.getPayload()));
                }

                @Override
                public void deliveryComplete(IMqttDeliveryToken token) {
                    System.out.println("Delivery complete");
                }
            });

            mqttClient.subscribe("poker/server");

            System.out.println("Server: Server listening...");

        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    public void stopServer() {
        try {
            if (mqttClient != null && mqttClient.isConnected()) {
                mqttClient.disconnect();
                System.out.println("Server: Server shutdown successful.");
            }
        } catch (MqttException e) {
            System.err.println("Server: Error shutting down server: " + e.getMessage());
        }
    }

    public static void main(String[] args) {
        ServeurPoker serveurPoker = new ServeurPoker();
        serveurPoker.LauchServer();
    }

    private void handleMessage(String topic, String message) {
        JSONObject jsonMessage = new JSONObject(message);
        String key = jsonMessage.getString("key");
        String nickname = jsonMessage.optString("nickname", null);

        System.out.println("Handling message with key: " + key);

        switch (key) {
            case "nickname":
                nickname = jsonMessage.getString("value");
                boolean pseudoValid = validateAndAddPlayer(nickname);
                JSONObject rep = new JSONObject();
                rep.put("key", "nickname");
                if (pseudoValid) {
                    rep.put("value", "Nickname = Available. Welcome to the table " + nickname);
                } else {
                    rep.put("value", "This nickname is already taken");
                }
                sendMessage("poker/client/" + nickname, rep.toString());
                break;
            case "createTable":
                String tableName = jsonMessage.getString("value");
                System.out.println("Creating table: " + tableName);
                createTable(tableName);
                break;
            case "joinTable":
                String tableToJoin = jsonMessage.getString("value");
                System.out.println("Joining table: " + tableToJoin + " with nickname: " + nickname);
                if (nickname != null) {
                    joinTable(tableToJoin, nickname);
                } else {
                    System.out.println("Nickname not provided for joining table.");
                }
                break;
            case "closeTable":
                String tableToClose = jsonMessage.getString("value");
                System.out.println("Closing table: " + tableToClose);
                closeTable(tableToClose);
                break;
            default:
                System.out.println("Unknown message key: " + key);
                break;
        }
    }

    private synchronized boolean validateAndAddPlayer(String nickname) {
        for (Table table : tables.values()) {
            for (Player player : table.getConnectedClient()) {
                if (player.getNickname() != null && player.getNickname().equalsIgnoreCase(nickname)) {
                    return false;
                }
            }
        }
        return true;
    }

    private void createTable(String tableName) {
        if (!tables.containsKey(tableName)) {
            tables.put(tableName, new Table(tableName));
            System.out.println("Table created: " + tableName);
        } else {
            System.out.println("Table already exists: " + tableName);
        }
    }

    private void joinTable(String tableName, String nickname) {
        if (tables.containsKey(tableName)) {
            Table table = tables.get(tableName);
            if (table.getConnectedClient().size() < PLAYERS_MAX) {
                Player newPlayer = new Player(nickname);
                table.getConnectedClient().add(newPlayer);
                System.out.println(nickname + " joined table: " + tableName);
                if (table.getConnectedClient().size() >= PLAYERS_MIN) {
                    startGame(table);
                }
            } else {
                System.out.println("Table is full: " + tableName);
            }
        } else {
            System.out.println("Table does not exist: " + tableName);
        }
    }

    private void closeTable(String tableName) {
        if (tables.containsKey(tableName)) {
            tables.remove(tableName);
            System.out.println("Table closed: " + tableName);
        } else {
            System.out.println("Table does not exist: " + tableName);
        }
    }

    private void startGame(Table table) {
        table.setPlayedGame(new Game(table.getConnectedClient()));
        table.getPlayedGame().startGame();
    }

    private void sendMessage(String topic, String message) {
        try {
            MqttMessage mqttMessage = new MqttMessage(message.getBytes());
            mqttMessage.setQos(2);
            mqttClient.publish(topic, mqttMessage);
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    public static class Player {
        private String nickname;
        private String play;
        private Hand hand;

        public Player(String nickname) {
            this.nickname = nickname;
            this.hand = new Hand();
        }

        public String getNickname() {
            return this.nickname;
        }

        public String getPlay() {
            return this.play;
        }

        public Hand getHand() {
            return this.hand;
        }
    }

    public static class Table {
        private String name;
        private List<Player> ConnectedClient;
        private Game playedGame;

        public Table(String name) {
            this.name = name;
            this.ConnectedClient = new ArrayList<>();
            this.playedGame = null;
        }

        public String getName() {
            return name;
        }

        public List<Player> getConnectedClient() {
            return ConnectedClient;
        }

        public Game getPlayedGame() {
            return playedGame;
        }

        public void setPlayedGame(Game playedGame) {
            this.playedGame = playedGame;
        }
    }
}
