package edu.info502.tp5.infra;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.JSONArray;
import org.json.JSONObject;

public class Game {
    private List<ServeurPoker.Player> players;
    private Talon talon;
    private List<Card> cartesCommunes;
    private MqttClient mqttClient;

    public Game(List<ServeurPoker.Player> players) {
        this.players = players;
        this.talon = new Talon();
        this.cartesCommunes = new ArrayList<>();
        try {
            this.mqttClient = new MqttClient("tcp://10.11.16.137:1883", MqttClient.generateClientId());
            MqttConnectOptions connOpts = new MqttConnectOptions();
            connOpts.setCleanSession(true);
            System.out.println("Game: Connecting to broker: " + mqttClient.getServerURI());
            mqttClient.connect(connOpts);
            System.out.println("Game: Connected");
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    public synchronized void startGame() {
        System.out.println("---------------------------------------------------------------");
        System.out.println("(Serveur)Début de la partie de Poker.");
        for (ServeurPoker.Player p : players) {
            JSONObject startGame = new JSONObject();
            startGame.put("key", "start");
            startGame.put("value", "La partie de Poker commence !");
            sendMessage(p.getNickname(), startGame.toString());
        }
        System.out.println("---------------------------------------------------------------");
        System.out.println("(Serveur)Distribution des cartes communes...");
        distribuerCartesPrivees();
        System.out.println("---------------------------------------------------------------");
        System.out.println("(Serveur)Flop");
        distribuerFlop();
        try {
            attendre(2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("---------------------------------------------------------------");
        System.out.println("(Serveur)Turn");
        distribuerTurn();
        try {
            attendre(2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("---------------------------------------------------------------");
        System.out.println("(Serveur)River");
        distribuerRiver();
        try {
            attendre(2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("---------------------------------------------------------------");
        System.out.println("(Serveur)Comparaison des mains");
        evaluerMainsJoueurs();
        try {
            attendre(2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("---------------------------------------------------------------");
        System.out.println("(Serveur)Annonce des résultats");
        determinerGagnant();
    }

    private void distribuerCartesPrivees() {
        System.out.println("(Serveur)Envoie de 2 cartes pour chaque joueur");
        for (int i = 0; i < 2; i++) {
            for (ServeurPoker.Player p : players) {
                p.getHand().ajouterCarte(talon.draw());
            }
        }
        for (ServeurPoker.Player p : players) {
            Hand handP = p.getHand();
            JSONObject hand = new JSONObject();
            JSONArray cardA = new JSONArray();
            for (Card card : handP.getCards()) {
                JSONObject cardJSON = new JSONObject();
                cardJSON.put("carte", card.toString());
                cardA.put(cardJSON);
            }
            hand.put("key", "hand");
            hand.put("value", cardA);
            sendMessage(p.getNickname(), hand.toString());
        }
        System.out.println("(Serveur)Toutes les cartes ont été distribuées aux joueurs !");
    }

    private void distribuerFlop() {
        for (int i = 0; i < 3; i++) {
            cartesCommunes.add(talon.draw());
        }
        for (ServeurPoker.Player p : players) {
            JSONObject flop = new JSONObject();
            JSONArray cardA = new JSONArray();
            for (Card card : cartesCommunes) {
                JSONObject cardJSON = new JSONObject();
                cardJSON.put("carte", card.toString());
                cardA.put(cardJSON);
            }
            flop.put("key", "flop");
            flop.put("value", cardA);
            sendMessage(p.getNickname(), flop.toString());
        }
    }

    private void distribuerTurn() {
        cartesCommunes.add(talon.draw());
        for (ServeurPoker.Player p : players) {
            JSONObject turn = new JSONObject();
            JSONArray cardA = new JSONArray();
            for (Card card : cartesCommunes) {
                JSONObject cardJSON = new JSONObject();
                cardJSON.put("carte", card.toString());
                cardA.put(cardJSON);
            }
            turn.put("key", "turn");
            turn.put("value", cardA);
            sendMessage(p.getNickname(), turn.toString());
        }
    }

    private void distribuerRiver() {
        cartesCommunes.add(talon.draw());
        for (ServeurPoker.Player p : players) {
            JSONObject river = new JSONObject();
            JSONArray cardA = new JSONArray();
            for (Card card : cartesCommunes) {
                JSONObject cardJSON = new JSONObject();
                cardJSON.put("carte", card.toString());
                cardA.put(cardJSON);
            }
            river.put("key", "river");
            river.put("value", cardA);
            sendMessage(p.getNickname(), river.toString());
        }
    }

    private void evaluerMainsJoueurs() {
        for (ServeurPoker.Player player : players) {
            Hand mainComplete = new Hand();
            mainComplete.Hand.addAll(player.getHand().Hand);
            mainComplete.Hand.addAll(cartesCommunes);
            mainComplete.Hand.sort((c1, c2) -> Integer.compare(c1.getChiffre(), c2.getChiffre()));

            String evaluation = mainComplete.eval();
            player.getHand().Hand = mainComplete.getCards();
            System.out.println(player.getHand().getCards().toString());

            JSONObject res = new JSONObject();
            res.put("key", "resultat");
            res.put("value", evaluation);
            sendMessage(player.getNickname(), res.toString());
        }
    }

    private void determinerGagnant() {
        ServeurPoker.Player meilleurJoueur = null;
        int meilleurScore = -1;

        for (ServeurPoker.Player player : players) {
            Hand hand = player.getHand();
            int scoreActuel = hand.evalScore();
            if (meilleurJoueur == null || scoreActuel > meilleurScore) {
                meilleurJoueur = player;
                meilleurScore = scoreActuel;
            } else if (scoreActuel == meilleurScore) {
                if (meilleurJoueur.getHand().evalKick() < hand.evalKick()) {
                    meilleurJoueur = player;
                }
            }
        }

        System.out.println("Le gagnant est " + meilleurJoueur.getNickname() + " avec le score : " + meilleurScore);
    }

    public void closeConnection() {
        for (ServeurPoker.Player player : players) {
            String value = "La partie est terminée, le serveur va se fermer!";
            JSONObject shut = new JSONObject();
            shut.put("key", "shutdown");
            shut.put("value", value);
            sendMessage(player.getNickname(), shut.toString());
        }
    }

    private void attendre(int secondes) throws InterruptedException {
        TimeUnit.SECONDS.sleep(secondes);
    }

    private void sendMessage(String nickname, String message) {
        try {
            MqttMessage mqttMessage = new MqttMessage(message.getBytes());
            mqttMessage.setQos(2);
            mqttClient.publish("poker/client/" + nickname, mqttMessage);
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }
}
