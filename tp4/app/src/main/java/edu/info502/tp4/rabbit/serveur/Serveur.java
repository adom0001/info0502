package edu.info502.tp4.rabbit.serveur;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;

import edu.info502.tp4.rabbit.commun.Player;
import edu.info502.tp4.rabbit.commun.PlayerManager;
import edu.info502.tp4.rabbit.commun.QCM;

/**
 * The Serveur class handles the server-side logic for managing QCMs and player interactions.
 */
public class Serveur {
    private static final String QUEUE_GENERALE = "queueDef";
    private static PlayerManager playerManager = new PlayerManager();

    /**
     * The main method to start the server.
     *
     * @param args Command line arguments.
     */
    public static void main(String[] args) {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("10.11.16.137");
        factory.setPort(5672);
        factory.setUsername("admin");
        factory.setPassword("admin");

        try (Connection connection = factory.newConnection();
             Channel channel = connection.createChannel()) {

            channel.queueDeclare(QUEUE_GENERALE, false, false, false, null);
            System.out.println("Listening on: " + QUEUE_GENERALE);

            DeliverCallback deliverCallback = (consumerTag, delivery) -> {
                try {
                    JSONObject js = new JSONObject(new String(delivery.getBody(), "UTF-8"));

                    if (js.has("type") && js.has("data") && js.has("idQCM")) {
                        String type = js.getString("type");
                        int idQCM = js.getInt("idQCM");
                        JSONObject p1 = js.getJSONObject("player");

                        switch (type) {
                            case "qcm":
                                envoieQCM(p1, idQCM, delivery, channel);
                                break;
                            case "reponses":
                                correctionQCM(p1, idQCM, delivery, channel);
                                break;
                            default:
                                System.out.println("Unsupported message type: " + type);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            };
            channel.basicConsume(QUEUE_GENERALE, true, deliverCallback, consumerTag -> {
            });

            synchronized (Serveur.class) {
                Serveur.class.wait();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sends a message to the client.
     *
     * @param type The type of the message.
     * @param qcm The QCM data.
     * @param reponse The response data.
     * @param p1 The player data.
     * @param correlationId The correlation ID.
     * @param replyQueueName The reply queue name.
     * @param channel The channel to send the message.
     * @throws Exception If there is an error sending the message.
     */
    public static void send(String type, JSONObject qcm, String reponse, JSONObject p1, String correlationId,
                            String replyQueueName, Channel channel) throws Exception {
        JSONObject rep = new JSONObject();
        rep.put("type", type);
        rep.put("data", new JSONObject());
        rep.put("user", p1);
        rep.getJSONObject("data").put("qcm", qcm);
        rep.getJSONObject("data").put("reponse", reponse);

        if (replyQueueName != null && correlationId != null) {
            AMQP.BasicProperties responseProps = new AMQP.BasicProperties.Builder()
                    .correlationId(p1.getString("id"))
                    .build();
            channel.basicPublish("", replyQueueName, responseProps, rep.toString().getBytes("UTF-8"));
        } else {
            throw new Exception("replyQueueName or correlationId not defined");
        }
    }

    /**
     * Sends a QCM to the player.
     *
     * @param p1 The player data.
     * @param idQCM The QCM ID.
     * @param delivery The delivery data.
     * @param channel The channel to send the QCM.
     * @throws Exception If there is an error sending the QCM.
     */
    public static void envoieQCM(JSONObject p1, int idQCM, com.rabbitmq.client.Delivery delivery, Channel channel)
            throws Exception {
        String idPlayer = p1.getString("id");
        String nom = p1.getString("surname");
        String prenom = p1.getString("name");
        String username = p1.getString("username");

        Player p2 = playerManager.getPlayer(String.valueOf(idPlayer));
        if (p2 == null) {
            p2 = playerManager.addPlayer(nom, prenom, username, 0, String.valueOf(idPlayer));
        }

        JSONObject qcmMessage = QCM.createQCMMessage(idQCM);
        JSONObject userInfo = new JSONObject();
        userInfo.put("id", idPlayer);
        userInfo.put("surname", nom);
        userInfo.put("name", prenom);
        userInfo.put("username", username);

        send("qcm", qcmMessage, "", userInfo, delivery.getProperties().getCorrelationId(), delivery.getProperties().getReplyTo(), channel);
    }

    /**
     * Corrects the QCM and sends the result to the player.
     *
     * @param p1 The player data.
     * @param idQCM The QCM ID.
     * @param delivery The delivery data.
     * @param channel The channel to send the result.
     * @throws Exception If there is an error correcting the QCM.
     */
    public static void correctionQCM(JSONObject p1, int idQCM, com.rabbitmq.client.Delivery delivery, Channel channel)
            throws Exception {
        String idPlayer = p1.getString("id");
        String nom = p1.getString("surname");
        String prenom = p1.getString("name");
        String username = p1.getString("username");

        JSONObject data = new JSONObject(new String(delivery.getBody(), "UTF-8")).getJSONObject("data");
        String tabRep = data.getString("reponses");

        int[] tableauReponses;
        try {
            JSONArray jsonArray = new JSONArray(tabRep);
            tableauReponses = new int[jsonArray.length()];
            for (int i = 0; i < jsonArray.length(); i++) {
                tableauReponses[i] = jsonArray.getInt(i);
            }
        } catch (JSONException e) {
            String[] tabArray = tabRep.split(",");
            tableauReponses = new int[tabArray.length];
            for (int i = 0; i < tabArray.length; i++) {
                tableauReponses[i] = Integer.parseInt(tabArray[i]);
            }
        }

        int[] reponsesCorrectes = null;
        if (idQCM == 1) {
            reponsesCorrectes = new int[]{1, 1, 1};
        } else if (idQCM == 2) {
            reponsesCorrectes = new int[]{1, 1, 2};
        } else if (idQCM == 3) {
            reponsesCorrectes = new int[]{4, 1, 2};
        }

        int score = QCM.corrigerQCM(tableauReponses, reponsesCorrectes);

        Player p2 = playerManager.getPlayer(idPlayer);
        if (p2 != null) {
            p2.setScore(p2.getScore() + score);
        }

        JSONObject userInfo = new JSONObject();
        userInfo.put("id", idPlayer);
        userInfo.put("surname", nom);
        userInfo.put("name", prenom);
        userInfo.put("username", username);
        userInfo.put("score", score);

        send("qcm_result", null, "", userInfo, delivery.getProperties().getCorrelationId(), delivery.getProperties().getReplyTo(), channel);
    }
}
