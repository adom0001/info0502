package edu.info502.tp4.rabbit.commun;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * The QCM class represents a multiple-choice questionnaire with questions and answers.
 */
public class QCM implements Serializable {
    private static final long serialVersionUID = 1L;

    private String[] questions = {
        "Qui est le primarch des ultramarines ?",
        "Quelle est la couleur des ultramarines ?",
        "Qui a peint la Joconde ?",
        "Combien il y a de jours dans 1 semaine ?",
        "Qui est Batman ?",
        "Combien je vais avoir à ce TP ?",
        "Qui est le plus con ?",
        "Quelles sont les couleurs des feux tricolores ?",
        "Est-ce la fin du QCM ?"
    };

    private String[][] reponses = {
        {"Roboute Guilliman", "Lion El'Jonson", "Sanguinius", "Horus"},
        {"Bleue", "Rouge", "Vert", "Noir"},
        {"Leonard de Vinci", "Michel-Ange", "Raphaël", "Donatello"},
        {"7", "5", "10", "8"},
        {"Bruce Wayne", "Clark Kent", "Tony Stark", "Peter Parker"},
        {"20/20", "0", "Je ne sais pas", "Autre"},
        {"Toi", "Moi", "Lui", "Personne"},
        {"Rouge, jaune, vert", "Bleu, rouge, vert", "Rouge, blanc, bleu", "Autre"},
        {"Oui", "Non", "Peut-être", "Je ne sais pas"}
    };

    private String question;
    private String[] options;

    /**
     * Constructs a QCM object with default values.
     */
    public QCM() {
    }

    /**
     * Constructs a QCM object with the specified question and options.
     *
     * @param question The question.
     * @param options The options for the question.
     */
    public QCM(String question, String[] options) {
        this.question = question;
        this.options = options;
    }

    /**
     * Constructs a QCM object from a JSON array of questions.
     *
     * @param questionsrecu The JSON array of questions.
     */
    public QCM(JSONArray questionsrecu) {
        this.questions = new String[questionsrecu.length()];
        this.reponses = new String[questionsrecu.length()][];

        for (int i = 0; i < questionsrecu.length(); i++) {
            JSONObject questionObj = questionsrecu.getJSONObject(i);
            this.questions[i] = questionObj.getString("question");
            JSONArray optionsArray = questionObj.getJSONArray("options");

            this.reponses[i] = new String[optionsArray.length()];
            for (int j = 0; j < optionsArray.length(); j++) {
                this.reponses[i][j] = optionsArray.getString(j);
            }
        }
    }

    /**
     * Allows the user to answer the QCM questions.
     *
     * @return An array of the user's answers.
     */
    public int[] repondreQCM() {
        int[] tabReponses = new int[this.questions.length];
        Scanner scanner = new Scanner(System.in);

        for (int i = 0; i < this.questions.length; i++) {
            System.out.println("Question " + (i + 1) + ": " + this.questions[i]);

            for (int j = 0; j < this.reponses[i].length; j++) {
                System.out.println((j + 1) + ". " + this.reponses[i][j]);
            }

            int choix = -1;
            while (choix < 1 || choix > this.reponses[i].length) {
                System.out.print("Votre réponse (1-" + this.reponses[i].length + "): ");
                if (scanner.hasNextInt()) {
                    choix = scanner.nextInt();
                } else {
                    scanner.next();
                    System.out.println("Veuillez entrer un nombre valide.");
                }
            }
            tabReponses[i] = choix;
        }
        return tabReponses;
    }

    /**
     * Gets the question.
     *
     * @return The question.
     */
    public String getQuestion() {
        return question;
    }

    /**
     * Gets the options for the question.
     *
     * @return The options.
     */
    public String[] getOptions() {
        return options;
    }

    /**
     * Gets a subset of questions by ID.
     *
     * @param idQCM The QCM ID.
     * @return A list of QCM objects.
     */
    public List<QCM> getQuestionsById(int idQCM) {
        List<QCM> qcmSubset = new ArrayList<>();

        int startIndex = (idQCM - 1) * 3;
        int endIndex = Math.min(startIndex + 3, questions.length);

        for (int i = startIndex; i < endIndex; i++) {
            qcmSubset.add(new QCM(questions[i], reponses[i]));
        }

        return qcmSubset;
    }

    /**
     * Converts the QCM object to a JSON object.
     *
     * @return The JSON representation of the QCM.
     */
    public JSONObject toJson() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("question", this.getQuestion());
        jsonObject.put("options", new JSONArray(this.getOptions()));
        return jsonObject;
    }

    /**
     * Converts a list of QCM objects to a JSON array.
     *
     * @param qcms The list of QCM objects.
     * @return The JSON array representation of the QCMs.
     */
    public static JSONArray toJsonArray(List<QCM> qcms) {
        JSONArray jsonArray = new JSONArray();
        for (QCM qcm : qcms) {
            jsonArray.put(qcm.toJson());
        }
        return jsonArray;
    }

    /**
     * Corrects the QCM based on the user's answers and the correct answers.
     *
     * @param reponsesJoueur The user's answers.
     * @param reponsesCorrectes The correct answers.
     * @return The score.
     * @throws IllegalArgumentException If the number of user answers does not match the number of questions.
     */
    public static int corrigerQCM(int[] reponsesJoueur, int[] reponsesCorrectes) {
        int score = 0;
        if (reponsesJoueur.length != reponsesCorrectes.length) {
            throw new IllegalArgumentException("Le nombre de réponses du joueur ne correspond pas au nombre de questions.");
        }
        for (int i = 0; i < reponsesJoueur.length; i++) {
            if (reponsesJoueur[i] == reponsesCorrectes[i]) {
                score++;
            }
        }
        return score;
    }

    /**
     * Creates a QCM message for a specific QCM ID.
     *
     * @param idQCM The QCM ID.
     * @return The JSON representation of the QCM message.
     */
    public static JSONObject createQCMMessage(int idQCM) {
        List<QCM> qcms = new QCM("", new String[0]).getQuestionsById(idQCM);
        JSONArray questionsJson = QCM.toJsonArray(qcms);

        JSONObject qcmMessage = new JSONObject();
        qcmMessage.put("type", "QCM");
        qcmMessage.put("questions", questionsJson);

        return qcmMessage;
    }
}
