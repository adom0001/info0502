package edu.info502.tp4.rabbit.commun;

import java.io.Serializable;

/**
 * The Player class represents a player with attributes such as surname, name, username, score, and ID.
 */
public class Player implements Serializable {
    private String surname;
    private String name;
    private String username;
    private int score;
    private String id;

    /**
     * Constructs a Player object with the specified attributes.
     *
     * @param surname The player's surname.
     * @param name The player's name.
     * @param username The player's username.
     * @param score The player's score.
     * @param id The player's ID.
     */
    public Player(String surname, String name, String username, int score, String id) {
        this.surname = surname;
        this.name = name;
        this.username = username;
        this.score = score;
        this.id = id;
    }

    /**
     * Gets the player's surname.
     *
     * @return The player's surname.
     */
    public String getSurname() {
        return surname;
    }

    /**
     * Gets the player's name.
     *
     * @return The player's name.
     */
    public String getName() {
        return name;
    }

    /**
     * Gets the player's username.
     *
     * @return The player's username.
     */
    public String getUsername() {
        return username;
    }

    /**
     * Gets the player's score.
     *
     * @return The player's score.
     */
    public int getScore() {
        return score;
    }

    /**
     * Gets the player's ID.
     *
     * @return The player's ID.
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the player's score.
     *
     * @param newScore The new score.
     */
    public void setScore(int newScore) {
        this.score = newScore;
    }

    /**
     * Returns a string representation of the player.
     *
     * @return A string representation of the player.
     */
    @Override
    public String toString() {
        return "Player Username: " + username +
               ", Player Surname: " + surname +
               ", Player Name: " + name +
               ", Player Score: " + score +
               ", Player id: " + id;
    }
}
