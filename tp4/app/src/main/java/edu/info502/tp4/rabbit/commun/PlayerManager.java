package edu.info502.tp4.rabbit.commun;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.security.Key;
import java.util.Base64;
import java.util.HashMap;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.spec.SecretKeySpec;

/**
 * The PlayerManager class manages player data, including adding, deleting, and retrieving players.
 * It also handles saving and loading player data to and from an encrypted file.
 */
public class PlayerManager {
    private HashMap<String, Player> players = new HashMap<>();
    private static final String FILE_NAME = "file_user_secu.txt";
    private static final String KEY_FILE = "secret.key";
    private Key secretKey;

    /**
     * Constructs a PlayerManager object and loads the encryption key.
     */
    public PlayerManager() {
        loadKey();
    }

    /**
     * Adds a player to the manager.
     *
     * @param surname The player's surname.
     * @param name The player's name.
     * @param username The player's username.
     * @param score The player's score.
     * @param id The player's ID.
     * @return The added Player object.
     */
    public Player addPlayer(String surname, String name, String username, int score, String id) {
        Player p = new Player(surname, name, username, score, id);
        players.put(p.getId(), p);
        savePlayer();
        return p;
    }

    /**
     * Deletes a player from the manager.
     *
     * @param id The player's ID.
     */
    public void deletePlayer(String id) {
        players.remove(id);
        savePlayer();
    }

    /**
     * Gets a player by ID.
     *
     * @param id The player's ID.
     * @return The Player object, or null if not found.
     */
    public Player getPlayer(String id) {
        return players.get(id);
    }

    /**
     * Gets all players.
     *
     * @return A HashMap of all players.
     */
    public HashMap<String, Player> getAllPlayer() {
        return players;
    }

    /**
     * Saves the players to an encrypted file.
     */
    public void savePlayer() {
        try {
            StringBuilder builder = new StringBuilder();
            for (Player p : players.values()) {
                builder.append(String.format("%s,%s,%s,%s,%d%n", p.getId(), p.getName(), p.getSurname(), p.getUsername(), p.getScore()));
            }
            String plainText = builder.toString();

            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            byte[] encryptedBytes = cipher.doFinal(plainText.getBytes("UTF-8"));
            String encryptedData = Base64.getEncoder().encodeToString(encryptedBytes);

            try (BufferedWriter writer = new BufferedWriter(new FileWriter(FILE_NAME))) {
                writer.write(encryptedData);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Loads all players from an encrypted file.
     */
    public void loadAllPlayers() {
        players.clear();
        File file = new File(FILE_NAME);
        if (!file.exists()) {
            return;
        }

        try {
            StringBuilder encryptedContent = new StringBuilder();
            try (BufferedReader reader = new BufferedReader(new FileReader(FILE_NAME))) {
                String line;
                while ((line = reader.readLine()) != null) {
                    encryptedContent.append(line);
                }
            }

            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.DECRYPT_MODE, secretKey);
            byte[] decryptedBytes = cipher.doFinal(Base64.getDecoder().decode(encryptedContent.toString()));
            String plainText = new String(decryptedBytes, "UTF-8");

            String[] lines = plainText.split("\n");
            for (String line : lines) {
                String[] parts = line.split(",");
                if (parts.length == 5) {
                    String id = parts[0];
                    String name = parts[1];
                    String surname = parts[2];
                    String username = parts[3];
                    int score = Integer.parseInt(parts[4]);
                    players.put(id, new Player(surname, name, username, score, id));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Loads or generates a secret key.
     */
    private void loadKey() {
        File keyFile = new File(KEY_FILE);
        if (keyFile.exists()) {
            try (BufferedReader reader = new BufferedReader(new FileReader(KEY_FILE))) {
                String keyString = reader.readLine();
                byte[] decodedKey = Base64.getDecoder().decode(keyString);
                secretKey = new SecretKeySpec(decodedKey, 0, decodedKey.length, "AES");
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            generateKey();
        }
    }

    /**
     * Generates a new secret key and saves it to a file.
     */
    private void generateKey() {
        try {
            KeyGenerator keyGen = KeyGenerator.getInstance("AES");
            keyGen.init(128);
            secretKey = keyGen.generateKey();

            try (BufferedWriter writer = new BufferedWriter(new FileWriter(KEY_FILE))) {
                String encodedKey = Base64.getEncoder().encodeToString(secretKey.getEncoded());
                writer.write(encodedKey);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
