package edu.info502.tp4.rabbit.client;

import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;

import org.json.JSONArray;
import org.json.JSONObject;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;

import edu.info502.tp4.rabbit.commun.QCM;

/**
 * Client class for interacting with the QCM server.
 */
public class Client {
    // Création des queues nécessaires
    private static final ConnectionFactory factory = new ConnectionFactory();
    private static final String QUEUE_GENERALE = "queueDef";
    private static String queuePerso = "";
    private static String correlationId = "";

    private static boolean enCour = true;
    private static boolean score = false;

    private static String nom;
    private static String prenom;
    private static String username;
    private static String idPlayer;
    private static int IdQCM;

    /**
     * Main method to start the client.
     *
     * @param args Command line arguments
     * @throws Exception if an error occurs
     */
    public static void main(String[] args) throws Exception {
        // Paramètres de connection à rabbitMQ
        factory.setHost("10.11.16.137");
        factory.setPort(5672);
        factory.setUsername("client_username");
        factory.setPassword("client_password");

        try (Connection connection = factory.newConnection();
             Channel channel = connection.createChannel()) {

            // Queue général pour envoyer les requetes au serveur
            channel.queueDeclare(QUEUE_GENERALE, false, false, false, null);

            // Queue perso pour les réponses à cette utilisateur
            queuePerso = channel.queueDeclare().getQueue();

            Scanner scanner = new Scanner(System.in);
            System.out.print("Entrez votre nom : ");
            nom = scanner.nextLine();
            System.out.print("Entrez votre prenom : ");
            prenom = scanner.nextLine();
            System.out.print("Entrez votre nom d'utilisateur : ");
            username = scanner.nextLine();
            idPlayer = connection.getAddress().getHostAddress() + prenom + nom;

            while (enCour) {
                System.out.print("Entrez le numéro de QCM que vous voulez recevoir (1, 2 ou 3) ou 'stop' pour arrêter : ");
                String input = scanner.nextLine();
                if (input.equalsIgnoreCase("stop")) {
                    enCour = false;
                    break;
                }
                try {
                    IdQCM = Integer.parseInt(input);
                    if (IdQCM < 1 || IdQCM > 3) {
                        System.out.println("Choix invalide. Veuillez entrer un nombre entre 1 et 3.");
                        continue;
                    }
                } catch (NumberFormatException e) {
                    System.out.println("Entrée invalide. Veuillez entrer un nombre entier.");
                    continue;
                }

                System.out.println("Vous avez sélectionné le QCM numéro : " + IdQCM);

                try {
                    qcm(channel);
                } catch (Exception prout) {
                    prout.printStackTrace();
                }

                DeliverCallback deliverCallBack = (consumerTag, delivery) -> {
                    if (delivery.getProperties().getCorrelationId().equals(idPlayer)) {
                        String mess = new String(delivery.getBody(), "UTF-8");
                        JSONObject js = new JSONObject(mess);
                        if (js.has("type") && js.has("user")) {
                            String type = js.getString("type");
                            JSONObject user = js.getJSONObject("user");
                            switch (type) {
                                case "qcm":
                                    // Extraire le tableau des questions depuis le champ "qcm"
                                    if (js.has("data") && js.getJSONObject("data").has("qcm")) {
                                        JSONObject qcmObject = js.getJSONObject("data").getJSONObject("qcm");
                                        if (qcmObject.has("questions")) {
                                            JSONArray tabDeQuestions = qcmObject.getJSONArray("questions");
                                            // Créer un objet QCM avec le JSONArray extrait
                                            QCM qcm = new QCM(tabDeQuestions);
                                            int tabReponses[] = qcm.repondreQCM();
                                            try {
                                                send(channel, "reponses", qcm, IdQCM, Arrays.toString(tabReponses));
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        } else {
                                            System.err.println("Le champ 'questions' est manquant dans l'objet 'qcm'.");
                                        }
                                    } else {
                                        System.err.println("Le champ 'qcm' est manquant dans les données.");
                                    }
                                    break;
                                case "qcm_result":
                                    System.out.println("Voici vos résultats suite au QCM : " + IdQCM);
                                    System.out.println("Résultats : " + user.getInt("score"));
                                    synchronized (Client.class) {
                                        Client.class.notify();
                                    }
                                    break;
                                default:
                                    System.out.println("Message inconnu");
                            }
                        }
                    }
                };

                channel.basicConsume(queuePerso, true, deliverCallBack, consumerTag -> {
                });

                synchronized (Client.class) {
                    Client.class.wait();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sends a QCM request to the server.
     *
     * @param channel The RabbitMQ channel
     * @throws Exception if an error occurs
     */
    public static void qcm(Channel channel) throws Exception {
        try {
            send(channel, "qcm", new QCM(), IdQCM, "");
        } catch (Exception paf) {
            paf.printStackTrace();
        }
    }

    /**
     * Sends a message to the server.
     *
     * @param channel The RabbitMQ channel
     * @param type The type of message
     * @param qcm The QCM object
     * @param idQCM The ID of the QCM
     * @param res The responses
     * @throws Exception if an error occurs
     */
    public static void send(Channel channel, String type, QCM qcm, int idQCM, String res) throws Exception {
        try {
            // Création d'une demande de QCM
            JSONObject request = new JSONObject();

            request.put("type", type);
            request.put("idQCM", idQCM);
            request.put("player", new JSONObject());
            request.getJSONObject("player").put("id", idPlayer);
            request.getJSONObject("player").put("surname", nom);
            request.getJSONObject("player").put("name", prenom);
            request.getJSONObject("player").put("username", username);

            request.put("data", new JSONObject());
            request.getJSONObject("data").put("reponses", res);
            request.getJSONObject("data").put("question", QCM.toJsonArray(qcm.getQuestionsById(idQCM)));

            // Envoi de la demande au serveur via QUEUE_QCM
            AMQP.BasicProperties properties = new AMQP.BasicProperties.Builder()
                    .correlationId(correlationId)
                    .replyTo(queuePerso)
                    .build();
            channel.basicPublish("", QUEUE_GENERALE, properties, request.toString().getBytes("UTF-8"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
